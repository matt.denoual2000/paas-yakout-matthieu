## Cluster de base de données - Présentation 

Le cluster de base de données est composé de deux machine s : 
* db-relationnal-active : 192.168.1.61
* db-relationnal-passive : 192.168.1.71

Les fichiers de configuration de ces machines sont dans les dossiers correspondants à leurs noms.

Le cluster a été fait avec repmgr sur postgresql 9.5
