package com.example.emergencynotificationagent.repository;

import com.example.emergencynotificationagent.model.Coach;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CoachRepository extends JpaRepository<Coach, Long> {

}
