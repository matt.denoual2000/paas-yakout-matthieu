package com.example.emergencynotificationagent.model;

import org.springframework.data.annotation.Id;

public class HeartRate {

    public HeartRate() {
    }

    @Id
    private String id;
    private int actualrate;

    public HeartRate(String id, int actualrate) {
        this.id = id;
        this.actualrate = actualrate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getActualrate() {
        return actualrate;
    }

    public void setActualrate(int actualrate) {
        this.actualrate = actualrate;
    }
}
