package com.example.emergencynotificationagent.model;


import jakarta.persistence.*;
import org.springframework.data.annotation.Id;

@Entity
@Table( name = "client")
public class Client {

    @jakarta.persistence.Id
    @Id
    Long id;

    @Column(name = "name")
    String name;

    @Column(name = "weight")
    int weight;

    @Column(name = "height")
    int height;

    @ManyToOne
    @JoinColumn(name="coach_id", nullable=false)
    private Coach coach;

    public Client() {
    }

    public Client(String name, int weight, int height, Coach coach) {
        this.name = name;
        this.weight = weight;
        this.height = height;
        this.coach = coach;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public Coach getCoach() {
        return coach;
    }

    public void setCoach(Coach coach) {
        this.coach = coach;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
