package com.example.emergencynotificationagent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmergencyNotificationAgentApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmergencyNotificationAgentApplication.class, args);
	}

}
