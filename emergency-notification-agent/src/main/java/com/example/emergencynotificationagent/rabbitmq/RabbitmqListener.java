package com.example.emergencynotificationagent.rabbitmq;


import com.example.emergencynotificationagent.model.HeartRate;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.MessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.amqp.core.Message;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

@Slf4j
@Service
public class RabbitmqListener implements MessageListener {
    @Autowired
    private final static String QUEUE_NAME = "data-emergency";
    ConnectionFactory factory = new ConnectionFactory();

    public void onMessage(Message message) {
        System.out.println("Consuming Message - " + new String(message.getBody()));
        ObjectMapper objectMapper = new ObjectMapper();
        HeartRate heartRate;
        try {
            heartRate = objectMapper.readValue(new String(message.getBody()), HeartRate.class);
            System.out.println("id : " + heartRate.getId() + " actualrate : " + heartRate.getActualrate());

            //heartRateRepository.save(heartRate);

        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

    }
}