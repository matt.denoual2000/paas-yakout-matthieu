package com.example.emergencynotificationagent.controller;

import com.example.emergencynotificationagent.model.Coach;
import com.example.emergencynotificationagent.repository.CoachRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/api/v1/Coach")
public class CoachController {
private final CoachRepository coachRepository;

    public CoachController(CoachRepository coachRepository) {
        this.coachRepository = coachRepository;
    }

    @GetMapping("/{id}")
    public ResponseEntity<Coach> getTutorialById(@PathVariable("id") long id) {
        Optional<Coach> coach = coachRepository.findById(id);

        if (coach.isPresent()) {
            return new ResponseEntity<>(coach.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
