package com.example.hrdataworker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HrDataWorkerApplication {

	public static void main(String[] args) {
		SpringApplication.run(HrDataWorkerApplication.class, args);
	}

}
