package com.example.hrdataworker.web.controller;

import com.example.hrdataworker.model.HeartRate;
import com.example.hrdataworker.repository.HeartRateRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
public class HeartRateController {
    private Logger logger =
            LoggerFactory.getLogger(HeartRateController.class);
    @Autowired
    private HeartRateRepository heartRateRepository;
    @GetMapping(value = "/HeartRate")
    public List<HeartRate> getAllHeartRate() {
        logger.info("Getting all HeartRate.");
        return heartRateRepository.findAll();
    }
    @GetMapping(value = "/{heartrateid}")
    public HeartRate getHeartRateById(@PathVariable String heartrateid) {
        logger.info("Getting EnergyDemand with ID: {}", heartrateid);
        return heartRateRepository.findHeartRateBy(heartrateid);
    }
    @PostMapping(value = "/create")
    public HeartRate addHeartRate(@RequestBody HeartRate heartRate) {
        logger.info("Saving heart rate.");
        return heartRateRepository.save(heartRate);
    }
    @PutMapping(value = "/update/{heartrateid}")
    public HeartRate updateHeartRate(@PathVariable String heartrateId, @RequestBody HeartRate heartRate) {
        logger.info("Updating Heart Rate with ID: {}", heartrateId);
        return heartRateRepository.save(heartRate);
    }
    @DeleteMapping(value = "/delete/{heartrateid}")
    public void deleteHeartRate(@PathVariable String heartrateId) {
        logger.info("Deleting Heart Rate with ID: {}", heartrateId);
        heartRateRepository.deleteById(heartrateId);
    }
}
