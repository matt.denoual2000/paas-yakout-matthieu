package com.example.hrdataworker.repository;


import com.example.hrdataworker.model.HeartRate;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface HeartRateRepository extends MongoRepository<HeartRate, String> {
    HeartRate findHeartRateBy(String heartrateid);
}